#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <errno.h>
#include <string.h>

//#include "disk_emulator.h"
#include "file_system.h"


#define NUM_OF_CLI_ARGS 3
#define FIRST_CLI_ARG 0

#define DEBUG 1
#define FORMAT 2
#define MOUNT 3
#define CAT 4
#define COPYIN 5
#define COPYOUT 6
#define HELP 7
#define EXIT 8
#define QUIT 9


void do_command_with_one_user_arg(int (*)(), char *, int);
void do_command_with_two_user_args(int (*)(char *, int), char *, int, int, char *);
void do_command_with_three_user_args(int (*)(char *, int), char *, int, int, char*, char*);
//static int do_copy_in(const char *, int);
//static int do_copy_out(const char *, int);
//void check_for_disk_initialization(char, char);
//void check_for_valid_argc(int, char *);

int extract_command(char *);
//void do_debug(int, char *, char *);
//void do_format(int, char *, char *);
//void do_mount(int, char *, char *);
//void do_cat(int, char *, char *);
int do_copy_out(char *, int);
int do_copy_in(char *, int);
void do_help();

//bool copyout(size_t, const char *);
//bool copyin(const char *, size_t);

//void(*func_ptr)(int, char *, char *);