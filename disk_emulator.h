#ifndef DISK_EMULATOR_H
#define DISK_EMULATOR_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#define DISK_MAGIC 0xdeadbeef
#define SIZE_OF_SINGLE_BLOCK 4096
#define SUCCESSFULLY_INITIALIZED 1

static FILE *diskfile;
static int amount_of_blocks=0;
static int nreads=0;
static int nwrites=0;

int  disk_init(const char *, int);
int  disk_size();
void disk_read(int, char *);
void disk_write(int, const char *);
void disk_close();

#endif