//-------------------------------------Include-------------------------------//

#include "disk_emulator.h"

//---------------------------------------------------------------------------//
//-------------------------------------Functions-----------------------------//
//---------------------------------------------------------------------------//

//-------------------------------------disk_init-----------------------------//
int disk_init(const char *filename, int n)
{
	diskfile = fopen(filename,"r+");
	if(!diskfile) 
        diskfile = fopen(filename,"w+");
	if(!diskfile) 
        return 0;

	ftruncate(fileno(diskfile),n*SIZE_OF_SINGLE_BLOCK);

	amount_of_blocks = n;
	nreads = 0;
	nwrites = 0;

	return SUCCESSFULLY_INITIALIZED;
}


//-------------------------------------disk_size-----------------------------//
int disk_size()
{
	return amount_of_blocks;
}


//-------------------------------------sanity_check--------------------------//
static void sanity_check(int block_address, const void *data)
{
	if(block_address < 0 ) 
    {
		printf("ERROR: blocknum (%d) is negative!\n",block_address);
		abort();
	}

	if(block_address >= amount_of_blocks) 
    {
		printf("ERROR: blocknum (%d) is too big!\n", block_address);
		abort();
	}

	if(data == NULL) 
    {
		printf("ERROR: null data pointer!\n");
		abort();
	}
}
 

//-------------------------------------disk_read-----------------------------//
void disk_read( int blocknum, char *data )
{
	sanity_check(blocknum,data);
    fseek(diskfile,blocknum*SIZE_OF_SINGLE_BLOCK,SEEK_SET);
    
    if(fread(data, SIZE_OF_SINGLE_BLOCK, 1, diskfile)==1)
		nreads++;
	else 
    {
		printf("ERROR: couldn't access simulated disk: %s\n",strerror(errno));
		abort();
	}
}


//-------------------------------------disk_write----------------------------//
void disk_write( int blocknum, const char *data )
{
	sanity_check(blocknum,data);
    fseek(diskfile,blocknum*SIZE_OF_SINGLE_BLOCK,SEEK_SET);

	if(fwrite(data, SIZE_OF_SINGLE_BLOCK, 1, diskfile)==1)
		nwrites++;
	else 
    {
		printf("ERROR: couldn't access simulated disk: %s\n",strerror(errno));
		abort();
	}
}


//-------------------------------------disk_close----------------------------//
void disk_close()
{
	if(diskfile) 
    {
		printf("%d disk block reads\n",nreads);
		printf("%d disk block writes\n",nwrites);
		fclose(diskfile);
		diskfile = 0;
	}
}
