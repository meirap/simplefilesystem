#ifndef FILE_SYSTEM_H
#define FILE_SYSTEM_H
//-------------------------------------INCLUDE-------------------------------//

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <stdbool.h>

#include "disk_emulator.h"
//-------------------------------------DEFINE--------------------------------//

#define FS_MAGIC_NUMBER 0xf0f03410
#define INODES_PER_BLOCK 128
#define AMOUNT_OF_DIRECT_POINTERS 5
#define POSSIBLE_AMOUNT_OF_POINTERS_PER_BLOCK 1024
#define TOTAL_AMOUNT_OF_BLOCKS 250
#define AMOUNT_OF_INODE_BLOCKS 25  //approx. 10% of total num of blocks
#define SIZE_OF_SINGLE_BLOCK 4096
#define AMOUNT_OF_BYTES 4
#define SUCCESSFULLY_FORMATTED 1
#define NOT_CREATED -1

//-------------------------------------DATA_STRUCTURES-----------------------//


typedef struct {
	int magic_number;
	int total_amount_of_blocks;
	int amount_of_inodeblocks;
	int total_amount_of_inodes;
}fs_superblock;

typedef struct {
	bool is_valid;
	int logical_size;
	int direct_pointers[AMOUNT_OF_DIRECT_POINTERS];
	int indirect_pointers;
}fs_inode;

typedef union {
	fs_superblock superblock;
	fs_inode inode[INODES_PER_BLOCK];
	int pointers[POSSIBLE_AMOUNT_OF_POINTERS_PER_BLOCK];
	char data[SIZE_OF_SINGLE_BLOCK];
}fs_block;



//void fs_debug();
int  fs_format();
int  fs_mount();
void check_if_already_mounted();
void check_inumber_validity(int);
int fs_read(int, char *, int, int);
int findFree();
int fs_write(int, const char *, int, int);
int fs_delete(int);
int fs_getsize(int);
int create();
void check_if_already_mounted();
void check_inumber_validity(int);
int copy_size(fs_inode, int, int);
int fs_read(int, char*, int, int);

#endif