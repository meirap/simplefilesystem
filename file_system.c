//------------------------------------INCLUDES---------------------------------------------------//


#include "file_system.h"


//-----------------------------------------------------------------------------------------------//
//-------------------------------------FS_FUNCTIONS----------------------------------------------//
//-----------------------------------------------------------------------------------------------//
int *bitmap = NULL; //initialized when mount
//-------------------------------------fs_mount--------------------------------------------------//
int fs_mount()
{
	fs_inode inode;
	fs_block datablock;
	fs_block block;
	int ninodeblocks = block.superblock.amount_of_inodeblocks;
	int nblocks = block.superblock.total_amount_of_blocks;
	int fileblocks;
	
	check_if_already_mounted();
	disk_read(0,block.data);

	bitmap = (int*)malloc(nblocks);
	memset(bitmap,0,nblocks);
	bitmap[0] = 1/*TAKEN*/;
	for(int i = 1; i <= ninodeblocks; i++){
		bitmap[i] = 1/*TAKEN*/;
		disk_read(i,block.data);
		for(int j = 0; j < INODES_PER_BLOCK; j++)
		{
			inode = block.inode[j];
			if(inode.is_valid)
			{
				fileblocks = inode.logical_size / SIZE_OF_SINGLE_BLOCK;
				if(inode.logical_size % SIZE_OF_SINGLE_BLOCK)
					fileblocks++;
				for(int k = 0; k < INODES_PER_BLOCK; k++)
					bitmap[inode.direct_pointers[k]] = 1/*TAKEN*/;
				if(fileblocks > INODES_PER_BLOCK)
				{
					disk_read(inode.indirect_pointers,datablock.data);
					for(int k = 0; k < (fileblocks-INODES_PER_BLOCK); k++)
						bitmap[datablock.pointers[k]] = 1/*TAKEN*/;
				}
			}
		}
	}
	return true;
}


//-------------------------------------fs_format-------------------------------------------------//
int fs_format()
{
    fs_block data;
    data.superblock.magic_number = FS_MAGIC_NUMBER;
    data.superblock.total_amount_of_blocks = TOTAL_AMOUNT_OF_BLOCKS;
    data.superblock.amount_of_inodeblocks = AMOUNT_OF_INODE_BLOCKS;
    data.superblock.total_amount_of_inodes = AMOUNT_OF_INODE_BLOCKS*INODES_PER_BLOCK;

	for(int i = 1; i < AMOUNT_OF_INODE_BLOCKS; i++)
	{
		fs_block block;
		for(int j = 0; j < INODES_PER_BLOCK; j++)
		{
			block.inode[j].is_valid = false;
			block.inode[j].logical_size = 0;
			memset(block.inode[j].direct_pointers, 0, AMOUNT_OF_DIRECT_POINTERS*AMOUNT_OF_BYTES);
			block.inode[j].indirect_pointers = 0;
		}
		disk_write(i+1, block.data);
	}
	return SUCCESSFULLY_FORMATTED;
}


//-------------------------------------fs_getsize------------------------------------------------//
int fs_getsize(int inumber)
{
	check_inumber_validity(inumber);

	int blocknum = (inumber-1) / INODES_PER_BLOCK+1;
	int inodenum = (inumber-1) % INODES_PER_BLOCK;
	
	fs_block block;
	disk_read(blocknum, block.data);
	fs_inode inode = block.inode[inodenum];
	if(inode.is_valid == 0)
	{
		printf("inumber is not valid. Not create yet.\n");
		return -1;
	}	
	return inode.logical_size;
}


//-------------------------------------fs_create-------------------------------------------------//
int fs_create()
{
	fs_block block;
	int inode_address;
	
	disk_read(0, block.data);
	for(int i = 0; i < AMOUNT_OF_INODE_BLOCKS; i++)
	{
		fs_block temporary_block;
		disk_read(i+1, temporary_block.data);
		for(int j = 0; j < INODES_PER_BLOCK; j++)
			if(!temporary_block.inode[j].is_valid)
			{
				temporary_block.inode[j].is_valid = true;
				temporary_block.inode[j].logical_size = 0;
				disk_write(i+1, temporary_block.data);
				disk_write(0, block.data);
				inode_address = i*INODES_PER_BLOCK+j+1;
				printf("created with an inumber of : %d", inode_address);
				return inode_address;
			}
	}
	return NOT_CREATED;
}


//-------------------------------------fs_delete-------------------------------------------------//
int fs_delete(int inumber)
{
	int blocknum = (inumber - 1) /INODES_PER_BLOCK + 1;
	int inodenum = (inumber - 1) %INODES_PER_BLOCK;
	fs_block block;
	fs_block superblock;
	fs_inode inode = block.inode[inodenum];
	
	check_if_already_mounted();
	disk_read(0, superblock.data);
	check_inumber_validity(inumber);
	disk_read(blocknum, block.data);

	if(inode.is_valid)
	{
		int fileblocks = inode.logical_size/SIZE_OF_SINGLE_BLOCK + ((inode.logical_size%SIZE_OF_SINGLE_BLOCK == 0)?0:1);
		for(int i = 0; i < INODES_PER_BLOCK; i++)
		{
			if(inode.direct_pointers[i] == 0)
				continue;
			bitmap[inode.direct_pointers[i]] = 0;
		}
		if(fileblocks > INODES_PER_BLOCK)
		{
			fs_block datablock;
			disk_read(inode.indirect_pointers, datablock.data);
			for(int k = 0; k < (fileblocks-INODES_PER_BLOCK); k++)
			{
				if(inode.direct_pointers[k] == 0)
					continue;
				bitmap[datablock.pointers[k]] = 0/*FREE*/;
			}
		}
		block.inode[inodenum].is_valid = 0;
		block.inode[inodenum].logical_size = 0;
		memset(block.inode[inodenum].direct_pointers, 0, INODES_PER_BLOCK * 4);
		block.inode[inodenum].indirect_pointers = 0;
		disk_write(blocknum, block.data);
		disk_write(0, superblock.data);
	}
	return 1;
}



//-------------------------------------fs_write--------------------------------------------------//
int fs_write( int inumber, const char *data, int length, int offset )
{
	check_inumber_validity(inumber);

	int blocknum = (inumber - 1) /INODES_PER_BLOCK + 1;
	int inodenum = (inumber - 1) %INODES_PER_BLOCK;
	
	fs_block block;
	int ret = 0;

	disk_read(0,block.data);
	if(block.superblock.magic_number == FS_MAGIC_NUMBER)
	{
		fs_block block;
		disk_read(blocknum, block.data);
		fs_inode inode = block.inode[inodenum];
		if(inode.is_valid)
		{
			//check the input
			if(inode.logical_size < offset)
				return ret;
			int beginning_of_block = offset/SIZE_OF_SINGLE_BLOCK;
			int block_offset = offset % SIZE_OF_SINGLE_BLOCK;
			int datablock_number = (length > (SIZE_OF_SINGLE_BLOCK-offset))?(length-(SIZE_OF_SINGLE_BLOCK-block_offset))/SIZE_OF_SINGLE_BLOCK : 0;
			int first_length = (length > SIZE_OF_SINGLE_BLOCK-block_offset) ? SIZE_OF_SINGLE_BLOCK-block_offset : length;	
			int last_block = (first_length < length)?(length - first_length) % SIZE_OF_SINGLE_BLOCK : 0;
	
			int direct_block_number = datablock_number+beginning_of_block+1;
			for(int i = 0; i < direct_block_number; i++)
			{
				if(inode.direct_pointers[i] != 0)
					continue;
				int freeblock = findFree();
				if(freeblock != -1)
				{
					bitmap[freeblock] = 1/*TAKEN*/;
					inode.direct_pointers[i] = freeblock;
				}
			}
			
			int extrablock = datablock_number+beginning_of_block+1-AMOUNT_OF_DIRECT_POINTERS;
			// allocate indirect pointer
			if(extrablock > 0 && inode.indirect_pointers == 0)
			{
				int freeblock = findFree();
				if(freeblock != -1)
				{
					bitmap[freeblock] = 1/*TAKEN*/;
					inode.indirect_pointers = freeblock;
				}
			}
			if(extrablock > 0)
			{
				fs_block indirect;
				disk_read(inode.indirect_pointers, indirect.data);
				// allocate space for data
				for(int k = 0; k < extrablock; k++)
				{
					if(indirect.pointers[k] != 0)
						continue;
					int tempfree = findFree();
					if(tempfree != -1)
					{
						bitmap[tempfree] = 1/*TAKEN*/;
						indirect.pointers[k] = tempfree;
					}
					else
						indirect.pointers[k] = 0;
				}
				disk_write(inode.indirect_pointers, indirect.data);
			}
			
			block.inode[inodenum] = inode;
			disk_write(blocknum, block.data);

			//write the first block
			fs_block indirect;
			disk_read(inode.indirect_pointers, indirect.data);
			fs_block datablock;
			int tempblocknum;
			if(beginning_of_block > AMOUNT_OF_DIRECT_POINTERS)
				tempblocknum = indirect.pointers[beginning_of_block-AMOUNT_OF_DIRECT_POINTERS];
			else
				tempblocknum = inode.direct_pointers[beginning_of_block];

			if(tempblocknum == 0)
			{
				block.inode[inodenum].logical_size += ret;
				disk_write(blocknum, block.data);
				return ret;
			}
			
			disk_read(tempblocknum, datablock.data);
			memcpy(datablock.data + block_offset, data, first_length);
			disk_write(inode.direct_pointers[beginning_of_block], datablock.data);
			ret += first_length;

			//write the rest block
			for(int i = 1; i <= datablock_number; i++){
				if(beginning_of_block + i < AMOUNT_OF_DIRECT_POINTERS)
				{
					if(inode.direct_pointers[beginning_of_block+i] == 0)
					{
						block.inode[inodenum].logical_size += ret;
						disk_write(blocknum, block.data);
						return ret;
					}
					disk_write(inode.direct_pointers[beginning_of_block+i], data+first_length+SIZE_OF_SINGLE_BLOCK*(i-1));
				}
				else
				{
					int temporary_block_number = indirect.pointers[beginning_of_block+i-AMOUNT_OF_DIRECT_POINTERS];
					if(temporary_block_number == 0)
					{
						block.inode[inodenum].logical_size += ret;
						disk_write(blocknum, block.data);
						return ret;
					}
					disk_write(temporary_block_number, data+first_length+SIZE_OF_SINGLE_BLOCK*(i-1));
				}
				ret += SIZE_OF_SINGLE_BLOCK;
			}
			
			if(last_block != 0)
			{
				fs_block temporary_block;
				
				if(beginning_of_block+datablock_number < AMOUNT_OF_DIRECT_POINTERS)
				{
					if(inode.direct_pointers[beginning_of_block+datablock_number] == 0)
					{
						block.inode[inodenum].logical_size += ret;
						disk_write(blocknum, block.data);
						return ret;
					}
					disk_read(inode.direct_pointers[beginning_of_block+datablock_number], temporary_block.data);
					memcpy(temporary_block.data, data+first_length+SIZE_OF_SINGLE_BLOCK*datablock_number, last_block);
					disk_write(inode.direct_pointers[beginning_of_block+datablock_number], temporary_block.data);
				}
				else
				{
					if(indirect.pointers[beginning_of_block+datablock_number-AMOUNT_OF_DIRECT_POINTERS] == 0)
					{
						block.inode[inodenum].logical_size += ret;
						disk_write(blocknum, block.data);
						return ret;
					}
					memcpy(temporary_block.data, data+first_length+SIZE_OF_SINGLE_BLOCK*datablock_number, last_block);
					disk_write(tempblocknum, temporary_block.data);
				}
				ret += last_block;
			}
			block.inode[inodenum].logical_size += ret;
			disk_write(blocknum, block.data);
		}
	}
	return ret;
}
//-------------------------------------help_functions--------------------------------------------//
int findFree()
{
	fs_block block;
	int nblocks = block.superblock.total_amount_of_blocks;
	int inodesblocks = block.superblock.amount_of_inodeblocks;
	
	check_if_already_mounted();	
	disk_read(0, block.data);

	for(int i = inodesblocks; i < nblocks; i++)
		if(bitmap[i] == 1 /*FREE*/)
			return i;
	return -1;
}



void check_if_already_mounted()
{
	if(bitmap != NULL)
	{
		printf("It has already been mounted!\n");
		return;
	}
} 


void check_inumber_validity(int inumber)
{
	fs_block superblock;
	disk_read(0, superblock.data);	
	int max_inode_adress = superblock.superblock.total_amount_of_inodes;
	if(inumber == 0 || inumber > max_inode_adress)
	{
		printf("The inumber is invalid!\n");
		return;
	}
}

int copy_size(fs_inode inode, int length, int offset) //POINTER????
{
	int copy_size;

	if(inode.logical_size-offset < length)
		copy_size = inode.logical_size-offset;
	else
		copy_size = length;
	
	return copy_size;
}

//-------------------------------------fs_read---------------------------------------------------//
int fs_read( int inumber, char *data, int length, int offset )
{	

	int block_address = (inumber-1) / INODES_PER_BLOCK+1;
	int inode_address = (inumber-1) % INODES_PER_BLOCK;
	fs_block block;
	
	check_if_already_mounted();
	check_inumber_validity(inumber);
	disk_read(0, block.data);
	
	if(block.superblock.magic_number == FS_MAGIC_NUMBER)
	{
		fs_block block;
		disk_read(block_address, block.data);
		fs_inode inode = block.inode[inode_address];
		if(inode.is_valid && !inode.logical_size < offset)
		{
			int copysize = copy_size(inode, length, offset);
			if(copysize > 0)
			{
				fs_block indirect;
				disk_read(inode.indirect_pointers, indirect.data);
				int blockbegin = offset / SIZE_OF_SINGLE_BLOCK;
				int blockoffset = offset % SIZE_OF_SINGLE_BLOCK;
				int datablocknum = (copysize > (SIZE_OF_SINGLE_BLOCK - offset)) ? (copysize - (SIZE_OF_SINGLE_BLOCK - blockoffset)) / SIZE_OF_SINGLE_BLOCK : 0;
				int first_length = (copysize > SIZE_OF_SINGLE_BLOCK - blockoffset) ? SIZE_OF_SINGLE_BLOCK - blockoffset : copysize;	
				int last_block = (copysize > first_length)?(copysize - first_length) % SIZE_OF_SINGLE_BLOCK : 0;

				fs_block first_datablock;
				if(blockbegin < INODES_PER_BLOCK)
					disk_read(inode.direct_pointers[blockbegin], first_datablock.data);
				else
					disk_read(indirect.pointers[blockbegin-INODES_PER_BLOCK], first_datablock.data);
				memcpy(data, first_datablock.data, first_length);

				//copy rest blocks
				for(int i = 1; i <= datablocknum; i++)
				{
					fs_block temporary_block;
					if(blockbegin+i < INODES_PER_BLOCK)
						disk_read(inode.direct_pointers[blockbegin + i], temporary_block.data);
					else
						disk_read(indirect.pointers[blockbegin+i-INODES_PER_BLOCK], temporary_block.data);
					memcpy(data+first_length+SIZE_OF_SINGLE_BLOCK*(i-1), temporary_block.data, SIZE_OF_SINGLE_BLOCK);
				}

				if(last_block != 0)
				{
					fs_block temporary_block;
					if(blockbegin + datablocknum + 1 < INODES_PER_BLOCK)
						disk_read(inode.direct_pointers[blockbegin+datablocknum+1], temporary_block.data);
					else
						disk_read(indirect.pointers[blockbegin+datablocknum+1-INODES_PER_BLOCK], temporary_block.data);
					memcpy(data+first_length+SIZE_OF_SINGLE_BLOCK*datablocknum, temporary_block.data, SIZE_OF_SINGLE_BLOCK);
				}
			}
			return copysize;
		}
	}
	return 0;
}
