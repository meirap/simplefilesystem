//-------------------------------------INCLUDES------------------------------//


#include "shell.h"


//-------------------------------------MAIN----------------------------------//
int main(int argc, char *argv[])
{
    char line[BUFSIZ], 
            cmd[BUFSIZ], 
            user_arg1[BUFSIZ], 
            user_arg2[BUFSIZ];
    int inumber, 
        result, 
        args;
    bool still_in_use_by_user = true;

    
    //check_for_valid_argc(argc, *argv);
    //check_for_disk_initialization(argv[1], argv[2]);
    while (still_in_use_by_user) 
    {
    	fprintf(stderr, "simpleFS> ");
    	fflush(stderr);

    	if (fgets(line, BUFSIZ, stdin) == NULL)
    	    break;

    	int args = sscanf(line, "%s %s %s", cmd, user_arg1, user_arg2);
    	if (args == 0)
    	    continue;

        int command = extract_command(cmd);
        switch(command)
        {/*
            case DEBUG: 
                do_command_with_one_user_arg(
                    *fs_debug,
                    "debug", 
                    args
                    ); 
                break;*/
            case FORMAT:
                do_command_with_one_user_arg(
                    *fs_format , 
                    "format", 
                    args
                    );
                break;
            case MOUNT:
                do_command_with_one_user_arg(
                    *fs_mount, 
                    "mount", 
                    args
                    );
                break;
            case CAT:
                do_command_with_two_user_args(
                    *do_copy_out,
                    "cat",
                    inumber,
                    args,
                    user_arg1
                );
                break;
            case COPYOUT:
                do_command_with_three_user_args(
                    *do_copy_out,
                    "copyout",
                    inumber,
                    args,
                    user_arg1,
                    user_arg2 
                );
                break;
            case COPYIN:
                do_command_with_three_user_args(
                    *do_copy_in,
                    "copyin",
                    inumber,
                    args,
                    user_arg2,
                    user_arg1  //order switched 
                );
                break;
            case EXIT:
            case QUIT:
                still_in_use_by_user = false;
                break;
            case HELP:
                do_help();
                break;
            default:
                printf("Unknown command: %s", line);
	            printf("Type 'help' for a list of commands.\n");
                break;
        }
	}
    return EXIT_SUCCESS;
}


//---------------------------------------------------------------------------//
//-------------------------------------COMMAND-FUNCTIONS---------------------//
//---------------------------------------------------------------------------//

//-------------------------------------extract_command-----------------------//
int extract_command(char *cmd)
{
    int command = 0;
    if (strcmp(cmd, "debug") == 0)
        command = DEBUG;
    if (strcmp(cmd, "format") == 0)
        command = FORMAT;
    if (strcmp(cmd, "mount") == 0)
        command = MOUNT;
    if (strcmp(cmd, "cat") == 0)
        command = CAT;
    if (strcmp(cmd, "copyin") == 0)
        command = COPYIN;
    if (strcmp(cmd, "copyout") == 0)
        command = COPYOUT;
    if (strcmp(cmd, "help") == 0)
        command = HELP;        
    if(strcmp(cmd, "exit") == 0 || strcmp(cmd, "quit") == 0)
        command = EXIT;
    
    return command;
}

//---------------------------------------------------------------------------//
//-------------------------------------do_commands----------------------------//

//-------------------------------------one-arg-------------------------------//
void do_command_with_one_user_arg(
    int (*command_ptr)(), 
    char *command_name, 
    int num_of_args
    ) 
{
    if (num_of_args != 1) 
    {
    	printf("Usage: %s\n", command_name);
    	return;
    }

    if (command_ptr != NULL)
        if (command_ptr) 
            printf("%s successfull.\n", command_name);
        else 
    	    printf("failed to %s!\n", command_name);
    else
        printf("No Command");
}

//-------------------------------------two-args------------------------------//
void do_command_with_two_user_args(
    int (*command_ptr)(char *, int), 
    char * command_name, 
    int inumber,
    int num_of_args,
    char * user_arg1
    )
{
    if(num_of_args == 2) 
			{
				inumber = atoi(user_arg1);
				if(!command_ptr("/dev/stdout", inumber)) 
					printf("%s failed!\n", command_name);
			} 
			else 
				printf("use: %s <inumber>\n", command_name);
}

//-------------------------------------three-args----------------------------//
void do_command_with_three_user_args(
    int (*command_ptr)(char *, int), 
    char * command_name,
    int inumber, 
    int num_of_args,
    char * user_arg1,
    char * user_arg2
    )
{
    if(num_of_args == 3) 
    {
		inumber = atoi(user_arg1);
		if(do_copy_in(user_arg2, inumber))
			printf("copied file %s to inode %d\n",user_arg2, inumber);
		else
			printf("copy failed!\n");
    }
	else
		printf("use: %s <filename> <inumber>\n", command_name);
}

//-------------------------------------do_help-------------------------------//
void do_help()
{
    printf("Commands are:\n");
			printf("    format\n");
			printf("    mount\n");
			printf("    debug\n");
			printf("    create\n");
			printf("    delete  <inode>\n");
			printf("    cat     <inode>\n");
			printf("    copyin  <file> <inode>\n");
			printf("    copyout <inode> <file>\n");
			printf("    help\n");
			printf("    quit\n");
			printf("    exit\n");
}


//-------------------------------------do_copy_in----------------------------//
int do_copy_in(char *filename, int inumber)
{
    FILE *file;
	int offset=0, 
        result, 
        actual;
	char buffer[16384];

	file = fopen(filename, "r");
	if(!file) 
    {
		printf("couldn't open %s: %s\n", filename, strerror(errno));
		return 0;
	}

	while(true) 
    {
		result = fread(buffer, 1, sizeof(buffer), file);
		if(result <= 0) 
            break;
		if(result > 0) 
        {
			actual = fs_write(inumber, buffer, result, offset);
			if(actual < 0) 
            {
				printf("ERROR: fs_write return invalid result %d\n",actual);
				break;
			}
			offset += actual;
			if(actual != result) 
            {
				printf("WARNING: fs_write only wrote %d bytes, not %d bytes\n",actual,result);
				break;
			}
		}
	}
    printf("%d bytes copied\n", offset);
    fclose(file);
	
    return 1;
}


//-------------------------------------do_copy_out---------------------------//
int do_copy_out(char *filename, int inumber)
{
    
	FILE *file;
	int offset=0, result;
	char buffer[16384];

	file = fopen(filename,"w");
	if(!file) 
    {
		printf("couldn't open %s: %s\n", filename, strerror(errno));
		return 0;
	}

	while(true) 
    {
		result = fs_read(inumber, buffer, sizeof(buffer), offset);
		if(result<=0) 
            break;
		fwrite(buffer, 1, result, file);
		offset += result;
	}

	printf("%d bytes copied\n",offset);
    fclose(file);
	
    return 1;
}

/*
//-------------------------------------help_functions------------------------//
void check_for_disk_initialization(char first_argv, char second_argv)
{
    if(!disk_init(first_argv, atoi(second_argv))) 
    {
		printf("couldn't initialize %d: %s\n", first_argv, strerror(errno));
		return;
	}
}

void check_for_valid_argc(int argc, char *argv)
{
    if (argc != NUM_OF_CLI_ARGS)
    {
    	fprintf(stderr, "Usage: %d <diskfile> <nblocks>\n", argv[FIRST_CLI_ARG]);
    	return;
    }   
}*/